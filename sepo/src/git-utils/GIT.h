#ifndef _GIT_WORKER_H
#define _GIT_WORKER_H

#include <string>

class GIT {
protected:
    std::string version;
public:
    GIT();
    std::string get_version();
    short verify_git_version(std::string minGitVersion);
    short check_ancestor(std::string inCommitA, std::string inCommitB);
};

#endif //_GIT_WORKER_H
