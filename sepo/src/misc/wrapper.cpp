#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>

#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <fstream>
#include "wrapper.h"

/* cut(): another strtok_r() in c++ */
std::vector<std::string> cut(const std::string inSrc, const std::string inPattern)
{
    std::vector<std::string> ret;
    std::string::size_type aPos;
    size_t aLen = inSrc.length();
    size_t bLen = inPattern.length();
    size_t aPre = std::string::npos;

    for (size_t i=0; i<aLen; ) {
        std::string aSlice;
        aPos = inSrc.find(inPattern, i);
        if (std::string::npos == aPos) {//search end, nothing found
            if (aPre == std::string::npos) {
                aSlice = inSrc;
            } else {
                aSlice = inSrc.substr(aPre + bLen);
            }
            i = aLen;//break;
        } else {
            if (std::string::npos == aPre) {
                aSlice = inSrc.substr(0, aPos);
            } else {
                aSlice = inSrc.substr(aPre + bLen, aPos - aPre - bLen);
            }
            aPre = aPos;
            i = aPos + bLen;
        }

        if (aSlice.length()) {
            ret.push_back(aSlice);
        }
    }

    return ret;
}

std::vector<std::string> readProfile(std::string inProfile)
{
    std::string aProfile;
    if (inProfile.size()) {
        struct passwd *pw = getpwuid(getuid());
        const char *homedir = pw->pw_dir;
        aProfile = std::string(homedir);
        aProfile += "/.sepo/";
        aProfile += inProfile;
    } else {
        aProfile = std::string(".repo/project.list");
    }

    std::vector<std::string> ret;
    std::string aLine;
    std::ifstream s1(aProfile); //need #include <fstream>
    if (s1.is_open()) {
        while( s1.good() ) {
            std::getline(s1, aLine);
#ifdef WRAPPER_MODULE_TEST
            printf ( "[%s] ", aLine.c_str());
#endif
            if(aLine.length() > 0 && '#' != aLine[0]) {
                ret.push_back(aLine);
#ifdef WRAPPER_MODULE_TEST
                printf ( "\n" );
#endif
            } else {
#ifdef WRAPPER_MODULE_TEST
                printf ( "ignored\n" );
#endif
            }
        }
        s1.close();
    } else {
        fprintf (stderr, "Can not open %s\n", aProfile.c_str());
    }

    return ret;
}

//using GNU and Mac extension: getcwd(nullptr...)
bool getcwd(std::string* s)
{
  char* cwd = getcwd(nullptr, 0);
  if (cwd != nullptr) *s = cwd;
  free(cwd);
  return (cwd != nullptr);
}

#ifdef WRAPPER_MODULE_TEST

void dump(std::string inSrc, std::vector<std::string> inVec)
{
    std::vector<std::string>::iterator aIt;
    std::cout << inSrc << "-->";
    for(aIt = inVec.begin(); aIt != inVec.end(); ++ aIt) {
        std::cout << "[" << *aIt << "],";
    }
    std::cout << std::endl;
}

int main ( int argc, char *argv[] )
{
    std::string aStr;
    std::string aPattern;
    std::vector<std::string> aRet;

    aStr = "abc_def__g";
    aPattern = "__";
    aRet = cut(aStr, aPattern);
    dump(aStr, aRet);

    aStr = "**AAA*A***A****BB******";
    aPattern = "**";
    aRet = cut(aStr, aPattern);
    dump(aStr, aRet);

    aStr = "";
    aPattern = "x";
    aRet = cut(aStr, aPattern);
    dump(aStr, aRet);

    readProfile("v4");

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */

#endif //WRAPPER_MODULE_TEST
