#ifndef __WRAPPER_H
#define __WRAPPER_H

#include <vector>
#include <string>

std::vector<std::string> cut(const std::string inSrc, const std::string inPattern);
std::vector<std::string> readProfile(std::string inProfile);
bool getcwd(std::string* s);

#endif //__WRAPPER_H
