#include "CMDrunner.h"
#include <stdio.h>
#include <string.h>
#include <cstdlib>
#include <unistd.h>
#ifdef _ANDROID_
#include <sys/wait.h> // WEXITSTATUS()
#endif

CMDrunner::CMDrunner()
:out(""), bSuccess(false), ret(-999)
{
}

CMDrunner::~CMDrunner()
{
}

void CMDrunner::dump()
{
    printf("CMDrunner [bSuccess=%d, ret=%d, out={%s}]\n", bSuccess, ret, out.c_str());
}

std::string CMDrunner::getOutput()
{
    return this->out;
}

int CMDrunner::getRet()
{
    return this->ret;
}

bool CMDrunner::good() {
    return this->bSuccess;
}

void CMDrunner::run(std::string cmd)
{
#if 0 // DEBUG
    printf ( "CMD: %s\n", cmd.c_str() );
#endif
    FILE *fp;
    char aDrop[1024];
    std::string buf = "";
    /* Open the command for reading. */
    cmd += " 2>&1";
    fp = popen(cmd.c_str(), "r");
    if (NULL == fp) {
      this->out = "Failed to run command";
      return;
    }
  
    /* Read the output a line at a time - output it. */
    while (fgets(aDrop, sizeof(aDrop)-1, fp) != NULL) {
      buf += aDrop;
    }
  
    this->bSuccess = true;
    int inner_ret = pclose(fp);
    this->ret = WEXITSTATUS(inner_ret);
    this->out = buf;
}

#ifdef CMDRUNNER_MODULE
int main ( int argc, char *argv[] )
{
    std::string theCmd = "ls /home";
    CMDrunner aRunner;
    aRunner.dump();
    aRunner.run(theCmd);
    aRunner.dump();
    theCmd = "fakecmd";
    aRunner.run(theCmd);
    aRunner.dump();

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
#endif
