#ifndef _WRAPPER_H
#define _WRAPPER_H

#include <string>

class CMDrunner {
protected:
    bool bSuccess;//0:fail, non-0:success
    int ret;
    std::string out;
public:
    CMDrunner();
    ~CMDrunner();
    void dump();
    void run(std::string cmd);
    std::string getOutput();
    int getRet();
    bool good();
};

#endif // _WRAPPER_H
