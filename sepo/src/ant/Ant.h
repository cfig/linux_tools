#ifndef __ANT_H
#define __ANT_H

#include <string>
#include <vector>

typedef struct
{
    int id;

    std::string basedir;
    std::vector<std::string> dirs;
    std::string cmd;
} Job_args;

void run_cmd_container(std::string inCmd, std::vector<std::string> inSubDirs, int inJobs);

#endif //__ANT_H
