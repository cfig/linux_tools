#include <cstdlib>
#include <cstdio>
#include <string>
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "SepoArgv.h"
#include "GIT.h"
#include "wrapper.h"
#include "Ant.h"

#define SEPO_VERSION PACKAGE_STRING

static void print_version() {
    printf ( SEPO_VERSION "\n");
}

int main(int argc, char** argv) {
    int ret = 88;
    GIT aWorker;
    aWorker.get_version();
    if (EXIT_FAILURE == aWorker.verify_git_version(MIN_GIT_VERSION)) {
        printf ("git version %s < %s\n", aWorker.get_version().c_str(), MIN_GIT_VERSION);
        exit(3);
    }
    SepoArgv *pArgs = new SepoArgv();
    bool bParsed = pArgs->parse(argc, argv);

    switch (pArgs->funcSelected) {
        case COMMANDS_NONE:
        case COMMANDS_HELP:
            pArgs->print_usage();
            ret = EXIT_SUCCESS;
            break;
        case COMMANDS_VERSION:
            ret = EXIT_SUCCESS;
            print_version();
            break;
        case COMMANDS_COMMAND:
            run_cmd_container(pArgs->theCmd, readProfile(pArgs->theProfile), pArgs->jobs);
            ret = EXIT_SUCCESS;
            break;
        case COMMANDS_ANCESTOR:
            if (2 != pArgs->theArgs.size()) {
                printf ( "sepo -a accepts exactly 2 parameters\n" );
                ret = 1;
            } else {
                ret = aWorker.check_ancestor(pArgs->theArgs[0], pArgs->theArgs[1]);
                ret = EXIT_SUCCESS;
                printf ( "\n" );
            }
            break;
        default:
            ret = 4;
            pArgs->print_usage();
            break;
    }

    return ret;
}
