#ifndef __SEPO_ARGV_H
#define __SEPO_ARGV_H

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string>
#include <vector>

enum commands_enum
{
    COMMANDS_NONE,
    COMMANDS_HELP,
    COMMANDS_COMMAND,
    COMMANDS_VERSION,
    COMMANDS_ANCESTOR
};                              /* ----------  end of enum commands_enum  ---------- */

typedef enum commands_enum Commands_enum;

/*
 * funcSelected: 0 - none
 * funcSelected: 1 - help
 * funcSelected: 2 - command
 * funcSelected: 3 - version
 */
class SepoArgv {
public:
    bool bDebug;
    int funcSelected;
    int jobs;
    std::string theCmd;
    std::string theProfile;
    std::vector<std::string> theArgs;
public:
    SepoArgv();
    bool parse(int argc, char** argv);
    void print_usage();
    ~SepoArgv() {};
private:
    void set_func(int);
};

#endif //__SEPO_ARGV_H
