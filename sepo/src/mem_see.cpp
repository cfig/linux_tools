#include "CMDrunner.h"
#include <stdio.h>
#include <string.h>
#include <cstdlib>
#include <unistd.h>

int main ( int argc, char *argv[] )
{
    std::string theCmd = "ps aux --sort:-rss | head";
    CMDrunner aRunner;
    aRunner.run(theCmd);
    aRunner.dump();
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
