#include "com_android_server_AlarmManagerService.h"

JNIEXPORT jlong JNICALL Java_com_android_server_AlarmManagerService_init
  (JNIEnv *, jobject) {
    return 0;
}

JNIEXPORT void JNICALL Java_com_android_server_AlarmManagerService_close
  (JNIEnv *, jobject, jlong) {
}

JNIEXPORT jint JNICALL Java_com_android_server_AlarmManagerService_set
  (JNIEnv *, jobject, jlong, jint, jlong, jlong) {
    return 0;
}

JNIEXPORT jint JNICALL Java_com_android_server_AlarmManagerService_waitForAlarm
  (JNIEnv *, jobject, jlong) {
    return 0;
}

JNIEXPORT jint JNICALL Java_com_android_server_AlarmManagerService_setKernelTime
  (JNIEnv *, jobject, jlong, jlong) {
    return 0;
}

JNIEXPORT jint JNICALL Java_com_android_server_AlarmManagerService_setKernelTimezone
  (JNIEnv *, jobject, jlong, jint) {
    return 0;
}

JNIEXPORT jlong JNICALL Java_com_android_server_AlarmManagerService_getNextAlarm
  (JNIEnv *, jobject, jlong, jint) {
    return 0;
}

