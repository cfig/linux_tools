package com.android.server

class AlarmManagerService {
    companion object {
        init {
            System.loadLibrary("alarm_manager")
        }
    }
    private external fun init(): Long
    private external fun close(nativeData: Long)
    private external operator fun set(nativeData: Long, type: Int, seconds: Long, nanoseconds: Long): Int
    private external fun waitForAlarm(nativeData: Long): Int
    private external fun setKernelTime(nativeData: Long, millis: Long): Int
    private external fun setKernelTimezone(nativeData: Long, minuteswest: Int): Int
    private external fun getNextAlarm(nativeData: Long, type: Int): Long
}
