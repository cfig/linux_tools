set nocompatible

let g:email = 'yuyezhong@gmail.com'
let g:user = 'cfig'

if exists('g:loaded_mess') || &compatible
  finish
else
  let g:loaded_mess = 'yes'
endif

if has('autocmd')
  filetype plugin indent on
endif
if has('syntax') && !exists('g:syntax_on')
  syntax enable
endif

"0 - no need to repeat the settings
set encoding=utf8
set hlsearch
set incsearch
set mouse=a "press <SHIFT> while selecting with mouse makes vim behaves as if mouse=
set nu
set autoindent
set smarttab
set expandtab
"显示tab
"set list
set listchars=tab:--
set listchars=tab:->,trail:-
set tabstop=4
set softtabstop=4
set shiftwidth=4
set fdm=syntax
"set fdc=4 "foldcolumn
set foldlevel=100 "启动时不要自动折叠代码
set vb t_vb= "diable bells

"set cul "highlight cursor's line
"set cursorcolumn
"set max line to 80
"set colorcolumn=90
colo desert

"色调改为85, 饱和度123, 亮度205
"HSV 128, 16, 93
"#C7EDCC
"RGB 199, 237, 204

let s:plugVimUrl = 'https://bitbucket.org/cfig/linux_tools/raw/master/install_vim_plugins/plug.vim'
if has('nvim')
  let s:plugVimFile = stdpath('config') . '/autoload/plug.vim'
  let s:plugVimDir = stdpath('config') . '/plugged'
  " aperezdc/vim-template
  let g:templates_directory = stdpath('config') . '/plugged/mess.vim/template_for_vim'
else
  let s:plugVimFile = '~/.vim/autoload/plug.vim'
  let s:plugVimDir = '~/.vim/plugged'
  " aperezdc/vim-template
  let g:templates_directory = '~/.vim/plugged/mess.vim/template_for_vim'
endif

if empty(glob(s:plugVimFile))
  if has('nvim')
    execute '!curl -fLo ' . stdpath('config') . '/autoload/plug.vim --create-dirs ' . s:plugVimUrl
  else
    let $VIMHOME = $HOME."/.vim"
    execute '!curl -fLo ' . $VIMHOME . '/autoload/plug.vim --create-dirs ' . s:plugVimUrl
  endif
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(s:plugVimDir)
function! BuildYCM(info)
  if a:info.status == 'installed' || a:info.force
    !./install.py --java-completer --clang-completer --rust-completer
  endif
endfunction
" project
Plug 'cfig/mess.vim'
" finder
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'mileszs/ack.vim'
Plug 'yegappan/mru'
Plug 'mhinz/vim-startify'
" git
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
" syntactic and semantic
Plug 'ycm-core/YouCompleteMe', { 'do': function('BuildYCM') }
Plug 'majutsushi/tagbar'
Plug 'gburca/vim-logcat'
Plug 'udalov/kotlin-vim'
Plug 'rust-lang/rust.vim'
" calc
Plug 'glts/vim-magnum'
Plug 'glts/vim-radical'
" colo
Plug 'vim-airline/vim-airline'
" from IdeaVim
Plug 'easymotion/vim-easymotion'
Plug 'machakann/vim-highlightedyank'
Plug 'tpope/vim-surround'
" other
Plug 'aperezdc/vim-template'
Plug 'skywind3000/vim-quickui'
call plug#end()

"1 - General
function! EnsureDirExists(inDir)
  if !isdirectory(a:inDir)
    call mkdir(a:inDir, 'p')
    echom "Created directory: " . a:inDir
  endif
endfunction

set backup
set backupdir=$HOME/.vim/backup//
call EnsureDirExists($HOME.'/.vim/backup')

" undo
"set undofile
"set undodir=$HOME/.vim/undo//
"call EnsureDirExists($HOME.'/.vim/undo')

set directory=$HOME/.vim/swap///
call EnsureDirExists($HOME.'/.vim/swap')

if has("cscope.files")
    set nonu
endif

"equiv to python platform.system()
function! GetOsInfo()
    if (has("win32") || has("win95") || has("win64") || has("win16"))
        return "Windows"
    elseif has("unix")
        return "Linux"
    elseif has("mac")
        return "Darwin"
    else
        return "UnknownOS"
    endif
endfunction

"http://vimdoc.sourceforge.net/htmldoc/gui.html
if has("gui_running")
    set lines=50 columns=84

    if has("linux")
        ":set guifont=Fira\ Code\ 14
        "https://www.jetbrains.com/lp/mono/#how-to-install
        :set guifont=JetBrains\ Mono\ 14
    elseif has("gui_win32")
        "set guifont=Fira\ Code\ 14
        "set guifont=Fira_code:h14:cANSI:qDRAFT
        :set guifont=JetBrains_Mono:h14:cANSI:qDRAFT
    elseif has("macunix")
        :set guifont=FiraCode-Regular:h18
        :set macligatures
    endif
endif

"ctags
if (has("win32") || has("win64"))
    let Tlist_Ctags_Cmd="c:/ctags.exe"
    "start SIS in vim
    nnoremap <silent> <F12> :!start "Insight3" -i  +<C-R>=expand(line("."))<CR> %<CR>
    "start vim in SIS
    "cmd: "C:\Program Files\Vim\vim73\gvim.exe" --remote-silent +%l %f
else
    let Tlist_Ctags_Cmd="/usr/bin/ctags"
endif

"taglist
if $SELECT_PROJECT_DB !=# ""
    "for ctags
    if filereadable(expand("~/.proj_db/$SELECT_PROJECT_DB/tags"))
        set tags=~/.proj_db/$SELECT_PROJECT_DB/tags;
    endif
else
    set tags=tags; " the ';' is a must!
endif

"shortcuts
nnoremap <F8> :TagbarToggle<CR>
"autocmd FileType c,cpp,make,python,java nested :TagbarOpen

:let mapleader= ","
nnoremap <leader>t :tabnew<CR>
nnoremap <leader>c :close<CR>
nnoremap <leader>sv :source $MYVIMRC<CR>
nnoremap <leader>" viw<esc>a"<esc>bi"<esc>lel
nnoremap <leader>cd :lcd %:p:h<CR>
nnoremap <silent> <S-Left> :tabp<CR>
nnoremap <silent> <S-Right> :tabn<CR>
nnoremap th :tabprevious<CR>
nnoremap tl :tabnext<CR>
nnoremap sc :e %:r.cpp<CR>
nnoremap sh :e %:r.h<CR>
"nnoremap vim :cd $VIMRUNTIME<CR>

"=============================================================================="
"                                   COMMENTS                                   "
"=============================================================================="
" -- ADD short-cuts for cscope --
nmap <leader>s :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap <leader>g :cs find g <C-R>=expand("<cword>")<CR><CR>
nmap <leader>e :cs find e <C-R>=expand("<cword>")<CR><CR>
nmap <leader>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
nmap <leader>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
nmap <leader>d :cs find d <C-R>=expand("<cword>")<CR><CR>
nmap <leader>p :g/<C-R>=expand("<cword>")<CR>/p<CR>
nmap <leader>m :vimgrep /<C-R>=expand("<cword>")<CR>/ %<CR>:copen<CR>
"show cscope in quickfix sub-window
"set cscopequickfix=s-,c-,d-,i-,t-,e-
"
"hex edit:
"  vim -b <file> : open file in binary mode
"  :%!xxd        : switch to hex dump
"  :%!xxd -r     : switch back from hex dump
"  :r !xxd -i <file>: read a dump of <file> formatted as C source
"
"File Format:
"convert from dos/unix to unix
"   :update
"   :e ++ff=dos
"   :setlocal ff=unix
"   :w
"0x0d: CR,^M mac
"0x0a: LF,^J,NL(newline) unix
"0x0d.0x0a: CRLF, dos

vmap gr y:vimgrep '<C-R>"' **<CR> :copen<CR>

"calendar.vim
let calendar_diary="~/work/i/imemory/vim_calendar"

" ack.vim
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif
cnoreabbrev ag Ack!

if has('gui_running')
    "set background=light
    set background=dark
    vnoremap <C-c> "+yi
else
    set background=dark
endif

nnoremap <silent> <F4> :Files!<CR>
nnoremap <silent> <F5> :Ag <C-R><C-W><CR>
nnoremap <leader>sv :source $MYVIMRC<cr>
iabbrev @@ yuyezhong@gmail.com
iab thedate <c-r>=strftime("=======20%y.%m.%d(%A)=======")<cr>

nnoremap <leader>" viw<esc>a"<esc>bi"<esc>lel

"nnoremap <leader>gf :YcmCompleter GoToDefinition<CR>
"nnoremap <leader>gl :YcmCompleter GoToDeclaration<CR>
nnoremap <leader>gg :YcmCompleter GoToDefinitionElseDeclaration<CR>
nnoremap <leader>gf :YcmCompleter GoToInclude<CR>

"parse log for json
" compiledb --parse out/verbose.log

"autocmd FileType make :setlocal list

"compare two vsp window
"windo diffthis

autocmd FileType vim setlocal shiftwidth=2 softtabstop=2 tabstop=2 expandtab

"gg=G  means retab current buffer
"
"
"neovim: python3 support

if !has('nvim')
set backspace=indent,eol,start
endif

"pip3 install --user --upgrade pynvim

"https://raw.githubusercontent.com/ycm-core/ycmd/master/.ycm_extra_conf.py

call quickui#menu#reset()

call quickui#menu#install('&File', [
            \ [ "&New File\tCtrl+n", 'echo 0' ],
            \ [ "&Open File\t(F3)", 'echo 1' ],
            \ [ "&Close", 'echo 2' ],
            \ [ "--", '' ],
            \ [ "&Save\tCtrl+s", 'echo 3'],
            \ [ "Save &As", 'echo 4' ],
            \ [ "Save All", 'echo 5' ],
            \ [ "--", '' ],
            \ [ "E&xit\tAlt+x", 'echo 6' ],
            \ ])
call quickui#menu#install('&Edit', [
            \ [ '&Copy', 'echo 1', 'help 1' ],
            \ [ '&Paste', 'echo 2', 'help 2' ],
            \ [ '&Find', 'echo 3', 'help 3' ],
            \ [ '&ToHex', '%!xxd -g1', 'help 3' ],
            \ [ '&FromHex', '%!xxd -r', 'help 3' ],
            \ ])

let g:quickui_show_tip = 1

nnoremap <silent> <F1> :call quickui#menu#open()<CR>
inoremap <silent> <F1> :call quickui#menu#open()<R>
vnoremap <silent> <F1> :call quickui#menu#open()<R>

function! Get_visual_selection()
    " Why is this not a built-in Vim script function?!
    let [line_start, column_start] = getpos("'<")[1:2]
    let [line_end, column_end] = getpos("'>")[1:2]
    let lines = getline(line_start, line_end)
    if len(lines) == 0
        return ''
    endif
    let lines[-1] = lines[-1][: column_end - (&selection == 'inclusive' ? 1 : 2)]
    let lines[0] = lines[0][column_start - 1:]
    echom join(lines, "\n")
    return join(lines, "\n")
endfunction

function! XSelection()
  try
    let a_save = @a
    normal! gv"ay
    return @a
  finally
    let @a = a_save
  endtry
endfunction

let g:airline_powerline_fonts = 1
"macro: paste macro contents of a: <C-r>a
"macro: paste macro contents of b: <C-r>b
let @l='0yf j0P0' " number lines
set history=200 "record last 200 commands in vim
