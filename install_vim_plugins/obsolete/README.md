
# Install
You need python3 and git to execute the script:

    curl -sL https://bitbucket.org/cfig/linux_tools/raw/master/install_vim_plugins/install_vim | python3

# external dependencies

cscope
exuberant-ctags
ag
fzf
youcompleteme
