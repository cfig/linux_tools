# Install

    wget https://bitbucket.org/cfig/linux_tools/raw/master/install_vim_plugins/vimrc -O ~/.vimrc

# external dependencies

```
apt install vim git vim-gtk  cscope exuberant-ctags fzf  silversearcher-ag neovim python3-dev
```

Remove
```
apt remove brltty
```
