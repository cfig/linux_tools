package ba

import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.exec.CommandLine
import org.apache.commons.exec.DefaultExecutor
import java.io.File
import kotlin.system.exitProcess

class BuildModules {
    private fun repoClean(inWorkdir: String) {
        val exec = DefaultExecutor()
        exec.workingDirectory = File(inWorkdir)
        exec.execute(CommandLine("repo")
                .addArgument("forall")
                .addArgument("-c")
                .addArgument("git")
                .addArgument("reset")
                .addArgument("--hard"))
        exec.execute(CommandLine("repo")
                .addArgument("forall")
                .addArgument("-c")
                .addArgument("git")
                .addArgument("clean")
                .addArgument("-xdf"))
    }

    private fun repoInit(prepareCode: PrepareCode) {
        val exec = DefaultExecutor()
        val adir = File(prepareCode.workDir)
        if (!adir.exists()) {
            adir.mkdirs()
        }
        exec.workingDirectory = adir
        exec.execute(CommandLine.parse(prepareCode.initCmd))
    }

    private fun repoSync(prepareCode: PrepareCode) {
        val exec = DefaultExecutor()
        exec.workingDirectory = File(prepareCode.workDir)
        exec.execute(CommandLine("repo")
                .addArgument("sync")
                .addArgument("-d")
                .addArgument("--force-sync"))
    }

    private fun cherryPick(inWorkdir: String, inGerritCmd: String) {
        DefaultExecutor().let {
            it.workingDirectory = File(inWorkdir)
            println("[CherryPick] " + inGerritCmd + ", in {" + it.workingDirectory.path + "}")
            it.execute(CommandLine.parse(inGerritCmd))
            it.execute(CommandLine.parse("git cherry-pick FETCH_HEAD"))
        }
    }

    fun readOrInitCfg(): AndroidBuildConfig {
        val cfg = "build.json"
        val cfgFile = File(cfg)
        val om = ObjectMapper().apply {
            configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)
        }

        if (!cfgFile.exists()) {
            //source code 1
            val pre1 = PrepareCode().apply {
                workDir = "android_source"
                bClean = true
                initCmd = "repo init -u https://android.googlesource.com/platform/manifest -b master"
                bInitAndSync = true
                cherryPicks.add("build,git fetch ssh://XX.XX refs/changes/84/65784/2")
            }

            //build
            val build1 = BuildCode().apply {
                workDir = pre1.workDir
                buildCmd = "m"
                args = mutableListOf("-j2")
            }

            //assemble
            val abc = AndroidBuildConfig().apply {
                pre = mutableListOf(pre1)
                build = build1
            }

            om.writerWithDefaultPrettyPrinter().writeValue(cfgFile, abc)
            println("Initial config written to $cfgFile")
            exitProcess(0)
        } else {
            return om.readValue(cfgFile, AndroidBuildConfig::class.java)
        }
    }

    fun prepareAndBuild(cfg: AndroidBuildConfig) {
        //prepare code
        for (item in cfg.pre) {
            if (item.bInitAndSync) {
                repoInit(item)
            }
            if (item.bInitAndSync || item.bClean) {
                repoClean(item.workDir)
            }
            if (item.bInitAndSync) {
                repoSync(item)
            }
        }

        for (item in cfg.pre) {
            for (c in item.cherryPicks) {
                val ret = c.split(",")
                cherryPick(item.workDir + "/" + ret[0], ret[1])
            }
        }

        for (item in cfg.pre) {
            for (c in item.postCmd) {
                val execPrep = DefaultExecutor()
                val ret = c.split(",")
                println(c)
                execPrep.workingDirectory = File(item.workDir + "/" + ret[0])
                execPrep.execute(CommandLine.parse(ret[1]))
            }
        }

        val cmd = CommandLine(cfg.build!!.buildCmd)
        for (arg in cfg.build!!.args!!) {
            cmd.addArgument(arg)
        }
        val exec = DefaultExecutor()
        exec.workingDirectory = File(cfg.build!!.workDir)
        exec.execute(cmd)
    }
}
