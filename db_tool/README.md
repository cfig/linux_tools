Requirements:

    kotlinc, the kotlin compiler

Install:
```shell
tf=`mktemp --dry-run --suffix=.kts` && wget https://bitbucket.org/cfig/linux_tools/raw/master/db_tool/inst.main.kts -O $tf && kotlinc -script $tf; rm -fv $tf;
```
