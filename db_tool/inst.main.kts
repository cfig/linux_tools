import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.PrintWriter
import java.net.URL
import java.nio.channels.Channels
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.attribute.PosixFilePermissions

data class DbConfig(
    val DB_MAGICCODE: String = "b58f0de491c311e5ac790021ccc95a85",
    var theInstallDir: File = File("")
) {
    fun init() {
        var thePrefix = System.getProperty("prefix")
        if (null == thePrefix) {
            thePrefix = System.getProperty("user.home")
        }
        theInstallDir = File(thePrefix, "/.programs/bin")
    }
}

fun getOsType(): String {
    val osName = System.getProperty("os.name").lowercase()
    return when {
        osName.indexOf("mac") >= 0 -> "darwin"
        osName.indexOf("linux") >= 0 -> "linux"
        else -> "na"
    }
}

fun getShellRc(): String {
    val shell = System.getenv("SHELL")
    val osType = getOsType()
    if (shell.endsWith("zsh")) {
        return "zshrc"
    } else if (shell.endsWith("bash")) {
        if (osType == "darwin") {
            return "bash_profile"
        } else {
            return "bashrc"
        }
    } else {
        throw RuntimeException("don't know which shell to use")
    }
}

fun chmod(fileName: String, modeStr: String) {
    println("chmod $fileName to mode $modeStr")
    Files.setPosixFilePermissions(
        Paths.get(fileName),
        PosixFilePermissions.fromString(modeStr)
    )
}

fun prepareDir(cfg: DbConfig) {
    if (!cfg.theInstallDir.exists()) {
        if (!cfg.theInstallDir.mkdirs()) {//fail to create
            throw RuntimeException(
                "Fail to create install dir: "
                        + cfg.theInstallDir.getAbsolutePath()
            )
        } else {
            println("$cfg.theInstallDir created")
        }
    } else {
        println("$cfg.theInstallDir already there")
    }
}

fun createNewShellRc(fileName: String = "shellrc", cfg: DbConfig) {
    val usrBashrc = File(System.getProperty("user.home"), "." + getShellRc())
    val generatedBashrc = File(fileName)

    if (!usrBashrc.exists()) {
        throw RuntimeException(usrBashrc.getPath() + " doesn't exist")
    } else {
        println("Found ${usrBashrc.getPath()}")
    }

    PrintWriter(generatedBashrc).let { pw ->
        usrBashrc.forEachLine { item ->
            if (!item.contains(cfg.DB_MAGICCODE)) {
                pw.println(item)
            } else {
                println("Found previous 'db_tool' config, updating ...")
            }
        }
        pw.println("# for db tools, MAGIC=" + cfg.DB_MAGICCODE)
        pw.println(
            "source " + File(cfg.theInstallDir.path, "db_envsetup").path
                    + " #" + cfg.DB_MAGICCODE
        )
        pw.close()
        println("$generatedBashrc created")
    }

    //replace with new shell.rc
    val usrBashrcBak = File(System.getProperty("user.home"), getShellRc() + ".bak")
    usrBashrcBak.delete()
    if (!usrBashrc.renameTo(usrBashrcBak)) {
        throw RuntimeException("Fail to backup " + usrBashrcBak.path)
    } else {
        println("Moving ${usrBashrc.getPath()} --> ${usrBashrcBak.path}")
    }

    println("Moving ${generatedBashrc.path} --> ${usrBashrc.path}")
    try {
        generatedBashrc.copyTo(usrBashrc, true)
        generatedBashrc.deleteOnExit()
    } catch (e: IOException) {
        println(e)
        throw RuntimeException("Fail to rewrite " + usrBashrc.path)
    }

    PrintWriter(File(cfg.theInstallDir.getAbsolutePath() + "/db_envsetup")).apply {
        println("alias dbc='. " + cfg.theInstallDir.path + "/dbcc'")
        println("alias dbgo='. " + cfg.theInstallDir.path + "/dbgo'")
        println("export PATH=" + cfg.theInstallDir.path + ":\$PATH")
        close()
    }
}

fun copyScripts(cfg: DbConfig) {
    println("Downloading script ...")
    val urlBase = "https://bitbucket.org/cfig/linux_tools/raw/master"
    nioDownload(
        "$urlBase/db_tool/src/db",
        "db")
    println("Copying files from 'db' --> " + cfg.theInstallDir.path)
    File("db").copyTo(File(cfg.theInstallDir, "/db"), overwrite = true)
    chmod(File(cfg.theInstallDir, "db").path, "rwxr-xr-x")
    File(cfg.theInstallDir, "dbcc").writeText(
        """
if [ "x" = "x$1" ]; then
    db l
else
    if [ -d "${"$"}HOME/.proj_db/$1" ]; then
        export SELECT_PROJECT_DB=$1
    else
        expCkSum=`echo -n $1 | md5sum | cut -f1 -d " "`
        bFound=0
        for item in `ls ${"$"}HOME/.proj_db/*/name`
        do
            actCkSum=`md5sum ${"$"}item | cut -f1 -d" "`
            if [ "${"$"}actCkSum" = "${"$"}expCkSum" ]; then
                bFound=1
                id=`echo ${"$"}item | cut -f5 -d"/"`
            fi
        done
        if [ "${"$"}bFound" = "1" ]; then
            export SELECT_PROJECT_DB=${"$"}id
        else
            echo "Invalil project ID: ${"$"}1"
            exit 1
        fi
    fi
fi
""".trim()
    )

    File(cfg.theInstallDir, "dbgo").writeText(
        """
if [ ! -z ${"$"}SELECT_PROJECT_DB ]; then
    cd ~/.proj_db/${"$"}SELECT_PROJECT_DB
fi
"""
    )
}

@Throws(IOException::class)
fun nioDownload(urlStr: String, file: String) {
    println("Downloading $urlStr -> $file ...")
    Channels.newChannel(URL(urlStr).openStream()).use { channel ->
        FileOutputStream(file).use { fos ->
            fos.channel.transferFrom(channel, 0, Long.MAX_VALUE)
        }
    }
    println("Downloading $urlStr -> $file done.")
}

fun vmain() {
    val config = DbConfig()
    config.init()
    println("Starting ...")
    prepareDir(config)
    copyScripts(config)
    createNewShellRc(cfg = config)
}

vmain()
