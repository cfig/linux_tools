defconfig := vs680_q_defconfig

linux_src := $(CURDIR)
linux_out := $(linux_src)/out
sdk_root := $(linux_src)/../
toolchain_root := $(sdk_root)/toolchain
$(warning tc $(toolchain_root))
$(warning linux_src $(linux_src))
PATH := $(toolchain_root)/aarch64/gcc-linaro-aarch64-linux-gnu-5.3/bin:$(toolchain_root)/aarch32/arm-linux-androideabi-4.9/bin:$(toolchain_root)/clang/linux-x86/clang-r353983c/bin:$(toolchain_root)/aarch64/gcc-arm-aarch64-elf-8.3/bin:$(toolchain_root)/aarch32/gcc-linaro-arm-eabi-6.2.1/bin:$(toolchain_root)/aarch32/arm-none-eabi-4.9/bin:$(PATH)
export PATH

CMD := ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- compiledb make CC=clang

linux:
	rm -fr $(linux_out) && mkdir -p $(linux_out)
	$(CMD) -C $(linux_src) O=$(linux_out) $(defconfig)
	$(CMD) -C $(linux_src) O=$(linux_out) -j 16
	cat $(linux_out)/arch/arm64/boot/Image | lz4 -f -9 -B4 > $(linux_out)/Image.lz4

# vim:ft=make
#
